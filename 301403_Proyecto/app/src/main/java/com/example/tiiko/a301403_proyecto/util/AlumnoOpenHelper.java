package com.example.tiiko.a301403_proyecto.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.tiiko.a301403_proyecto.model.Alumno;

import java.util.ArrayList;

public class AlumnoOpenHelper extends SQLiteOpenHelper {
    private static final String ALUMNOS_TABLE_CREATE =
            "CREATE TABLE alumnos(matricula TEXT PRIMARY KEY, nombre_alumno TEXT, apellido_paterno TEXT, apellido_materno TEXT)";
    private static final String DB_NAME =
            "uachistencias.sqlite";
    private static final int DB_VERSION = 1;
    private SQLiteDatabase db;

    public AlumnoOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db=this.getWritableDatabase();
        db=this.getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ALUMNOS_TABLE_CREATE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Insertar un nuevo Alumno
    public void insertarAlumno(int clave_grupo, String nombre_materia, String porcentaje_faltas, String horas_totales){
        ContentValues cv = new ContentValues();
        cv.put("matricula", clave_grupo);
        cv.put("nombre_alumno", nombre_materia);
        cv.put("apellido_paterno", porcentaje_faltas);
        cv.put("apellido_materno", horas_totales);
        db.insert("alumnos", null, cv);
    }

    //Borrar un Alumno a partir de su id
    public void borrar(String id){
        String[] args = new String[]{id};
        db.delete("alumnos", "matricula=?", args);
    }

    //Obtener la lista de Alumnos en la base de datos
    public ArrayList<Alumno> getAlumnos(){
        //Creamos el cursor
        ArrayList<Alumno> listaAlumnos=new ArrayList<Alumno>();
        Cursor c = getReadableDatabase().rawQuery("select matricula, nombre_alumno, apellido_paterno, apellido_materno from alumnos", null);
        if (c != null && c.getCount()>0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Alumno
                int matricula = c.getInt(c.getColumnIndex("matricula"));
                String nombre_alumno = c.getString(c.getColumnIndex("nombre_alumno"));
                String apellido_paterno = c.getString(c.getColumnIndex("apellido_paterno"));
                String apellido_materno = c.getString(c.getColumnIndex("apellido_materno"));
                Alumno alumno =new Alumno(matricula, nombre_alumno, apellido_paterno, apellido_materno);
                //Añadimos el Alumno a la lista
                listaAlumnos.add(alumno);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listaAlumnos;
    }

}