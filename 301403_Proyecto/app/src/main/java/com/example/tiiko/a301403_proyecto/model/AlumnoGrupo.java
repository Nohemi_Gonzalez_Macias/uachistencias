package com.example.tiiko.a301403_proyecto.model;

public class AlumnoGrupo {

    /*private Alumno alumno;
    private Grupo grupo;

    public AlumnoGrupo(Alumno alumno, Grupo grupo) {
        this.alumno = alumno;
        this.grupo = grupo;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }*/

    private int matricula;
    private String clave_grupo;

    public AlumnoGrupo() {
    }

    public AlumnoGrupo(String clave_grupo) {
        this.clave_grupo = clave_grupo;
    }

    public AlumnoGrupo(int matricula, String clave_grupo) {
        this.matricula = matricula;
        this.clave_grupo = clave_grupo;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getClave_grupo() {
        return clave_grupo;
    }

    public void setClave_grupo(String clave_grupo) {
        this.clave_grupo = clave_grupo;
    }

    //Represetacion del objeto como cadena de texto
    public String toString() {
        //String nombre_alumno;
       // nombre_alumno = alumno.getNombre() + " " + alumno.getApellido_paterno() + " " + alumno.getApellido_materno();
        return String.valueOf(matricula);
    }
}
