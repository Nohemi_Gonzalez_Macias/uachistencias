package com.example.tiiko.a301403_proyecto.model;

public class Grupo {

    private String clave_grupo;
    private String nombre_materia;
    private int porcentaje_faltas;
    private int horas_totales;

    public Grupo(String clave_grupo, String nombre_materia, int porcentaje_faltas, int horas_totales) {
        this.clave_grupo = clave_grupo;
        this.nombre_materia = nombre_materia;
        this.porcentaje_faltas = porcentaje_faltas;
        this.horas_totales = horas_totales;
    }

    public Grupo(String clave_grupo) {
        this.clave_grupo = clave_grupo;
    }

    public String getClave_grupo() {
        return clave_grupo;
    }

    public void setClave_grupo(String clave_grupo) {
        this.clave_grupo = clave_grupo;
    }

    public String getNombre_materia() {
        return nombre_materia;
    }

    public void setNombre_materia(String nombre_materia) {
        this.nombre_materia = nombre_materia;
    }

    public int getPorcentaje_faltas() {
        return porcentaje_faltas;
    }

    public void setPorcentaje_faltas(int porcentaje_faltas) {
        this.porcentaje_faltas = porcentaje_faltas;
    }

    public int getHoras_totales() {
        return horas_totales;
    }

    public void setHoras_totales(int horas_totales) {
        this.horas_totales = horas_totales;
    }

    //Represetacion del objeto como cadena de texto
    @Override
    public String toString() {
        return nombre_materia;
    }
}
