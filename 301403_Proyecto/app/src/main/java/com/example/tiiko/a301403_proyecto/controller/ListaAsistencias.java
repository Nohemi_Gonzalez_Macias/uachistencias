package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.AlumnoGrupo;
import com.example.tiiko.a301403_proyecto.model.Asistencia;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class ListaAsistencias extends AppCompatActivity {

    private TextView textClaveGrupo;
    private Button buttonVerAlumnos;
    private String nombreMateria;
    private Button buttonAgregarAsistencia;
    private Button buttonVerGrupos;
    private ListView listaAsistencias;

    // Elementos para poder manjear la lista
    private ArrayList<Asistencia> arrayAsistencia;
    private ArrayAdapter adapterAsistencia;
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asistencia);

        Bundle bundle = getIntent().getExtras();
        nombreMateria = bundle.getString("nombreMateria");

        textClaveGrupo=(TextView)findViewById(R.id.textListaAsistencia);
        textClaveGrupo.setText(nombreMateria);

        buttonVerAlumnos=(Button)findViewById(R.id.buttonVerAlumnos);
        buttonVerGrupos=(Button)findViewById(R.id.buttonVerGrupos);
        buttonAgregarAsistencia=(Button)findViewById(R.id.buttonAgregarAsistencia);
        listaAsistencias=(ListView)findViewById(R.id.listAsistencias) ;

        db = new OpenHelper(this);

        if(!db.obtenerAsistencia(textClaveGrupo.getText().toString()).isEmpty()){
            arrayAsistencia = db.obtenerAsistencia(textClaveGrupo.getText().toString());

            adapterAsistencia = new ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    db.obtenerAsistencia(textClaveGrupo.getText().toString())
            );

            listaAsistencias.setAdapter(adapterAsistencia);
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), R.string.toastNoAlumnos, Toast.LENGTH_LONG);
            toast.show();
        }

        buttonAgregarAsistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityFormularioAsistencia = new Intent(v.getContext(), FormularioAsistencia.class);
                activityFormularioAsistencia.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityFormularioAsistencia,0);
            }
        });

        buttonVerGrupos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityGrupos = new Intent(v.getContext(), Grupos.class);
                startActivityForResult(activityGrupos, 0);
            }
        });

        buttonVerAlumnos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityListaAlumnos = new Intent(v.getContext(), ListaAlumnos.class);
                activityListaAlumnos.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityListaAlumnos,0);
            }
        });

    }
}
