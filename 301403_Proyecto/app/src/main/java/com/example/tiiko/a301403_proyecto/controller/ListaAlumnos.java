package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiiko.a301403_proyecto.ListViewSubitem;
import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.Alumno;
import com.example.tiiko.a301403_proyecto.model.AlumnoGrupo;
import com.example.tiiko.a301403_proyecto.model.Asistencia;
import com.example.tiiko.a301403_proyecto.model.Grupo;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class ListaAlumnos extends AppCompatActivity {

    private Button buttonAgregarAlumno;
    private Button buttonVerAsistencias;
    private ListView listaAlumnos;
    private TextView textNombreMateria;
    String nombreMateria;

    // Elementos para poder manjear la lista
    private ArrayList<AlumnoGrupo> arrayAlumnos;
    private ArrayAdapter adapterAlumnos;
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alumnos);

        Bundle bundle = getIntent().getExtras();
        nombreMateria = bundle.getString("nombreMateria");

        buttonAgregarAlumno = ( Button ) findViewById(R.id.buttonAgregarAlumno);
        buttonVerAsistencias=(Button)findViewById(R.id.buttonVerAsistencias);
        listaAlumnos = ( ListView ) findViewById(R.id.listAlumnos);
        textNombreMateria = ( TextView ) findViewById(R.id.textNombreMateria);

        textNombreMateria.setText(nombreMateria);

        db = new OpenHelper(this);

        if (!db.obtenerAlumnos(nombreMateria).isEmpty()) {
            arrayAlumnos = db.obtenerAlumnos(nombreMateria);

            adapterAlumnos = new ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    db.obtenerAlumnos(nombreMateria)
            );

            listaAlumnos.setAdapter(adapterAlumnos);
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), R.string.toastNoAlumnos, Toast.LENGTH_LONG);
            toast.show();
        }

        buttonAgregarAlumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityFormularioAlumno = new Intent(v.getContext(), FormularioAlumno.class);
                activityFormularioAlumno.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityFormularioAlumno, 0);
            }
        });

        buttonVerAsistencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityListaAsistencias = new Intent(v.getContext(), ListaAsistencias.class);
                activityListaAsistencias.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityListaAsistencias, 0);
            }
        });

        listaAlumnos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int totalFaltasPorAlumno = db.obtenerFaltasPorAlumno();
                int faltasPermitidasPorMateria = db.obtenerFaltasPermitidasPorMateria();
                if(totalFaltasPorAlumno > faltasPermitidasPorMateria){
                    Toast toast = Toast.makeText(getApplicationContext(), "Reprobado por faltas. Faltas totales: " + totalFaltasPorAlumno, Toast.LENGTH_LONG);
                    toast.show();
                }

                //finish();
            }
        });

    }
}
