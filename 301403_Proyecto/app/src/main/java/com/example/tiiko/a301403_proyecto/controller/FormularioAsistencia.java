package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.Asistencia;
import com.example.tiiko.a301403_proyecto.model.Grupo;
import com.example.tiiko.a301403_proyecto.util.DeleteItemDialog;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class FormularioAsistencia extends AppCompatActivity {

    private Spinner spinnerTipoAsistencia;
    private Button buttonAgregarAsistencia;
    private Button buttonVerAsistencias;
    private Button buttonVerGrupos;
    private EditText editFechaAsistencia;
    //private EditText editMatricula;
    private EditText editClaveGrupo;
    private Spinner spinnerMatricula;
    private ArrayAdapter spinnerAdapterMatriculas;
    String nombreMateria;

    //Lista de grupos y grupo actual
    private ArrayList<Asistencia> listaAsistencia;
    private Asistencia asistencia;

    //Controlador de bases de datos
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_asistencia);

        Bundle bundle = getIntent().getExtras();
        nombreMateria = bundle.getString("nombreMateria");

        String[] items = new String[] {"Asistencia", "Falta"};
        spinnerTipoAsistencia = (Spinner) findViewById(R.id.spinnerTipoAsistencia);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTipoAsistencia.setAdapter(adapter);

        buttonAgregarAsistencia=(Button)findViewById(R.id.buttonAgregarAsistencia);
        buttonVerAsistencias=(Button)findViewById(R.id.buttonVerAsistencias);
        buttonVerGrupos=(Button)findViewById(R.id.buttonVerGrupos);
        editFechaAsistencia=(EditText) findViewById(R.id.editFechaAsistencia);
      //  editMatricula=(EditText)findViewById(R.id.editMatricula);
        editClaveGrupo=(EditText)findViewById(R.id.editClaveGrupo);
        spinnerMatricula=(Spinner)findViewById(R.id.spinnerMatricula);

        editClaveGrupo.setText(nombreMateria);
        editClaveGrupo.setEnabled(false);

        //Iniciamos el controlador de la base de datos
        db=new OpenHelper(this);

        //Creamos el adapter y lo asociamos al spinner
        spinnerAdapterMatriculas=new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item, db.obtenerMatriculas());
        spinnerMatricula.setAdapter(spinnerAdapterMatriculas);
        //spinnerMatricula.setOnItemSelectedListener(this);

        // Agregar listeners
        buttonAgregarAsistencia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.insertarAsistencia(
                        editFechaAsistencia.getText().toString(),
                        spinnerTipoAsistencia.getSelectedItem().toString(),
                        Integer.parseInt(spinnerMatricula.getSelectedItem().toString()),
                        editClaveGrupo.getText().toString());
                editFechaAsistencia.setText("");
             //   editMatricula.setText("");
                editClaveGrupo.setText("");
            }
        });

        buttonVerAsistencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityListaAsistencias = new Intent(v.getContext(), ListaAsistencias.class);
                activityListaAsistencias.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityListaAsistencias, 0);
            }
        });

        buttonVerGrupos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityGrupos = new Intent(v.getContext(), Grupos.class);
                startActivityForResult(activityGrupos, 0);
            }
        });

    }
}
