package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.Grupo;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class Grupos extends AppCompatActivity {

    private Button buttonAgregarGrupo;
    private ListView listaGrupos;

    // Elementos para poder manjear la lista
    private ArrayList<Grupo> arrayGrupos;
    private ArrayAdapter adapterGrupo;
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grupos);

        //Inicializamos los elementos de la interfaz
        listaGrupos=(ListView ) findViewById(R.id.listGrupos);
        buttonAgregarGrupo =(Button ) findViewById(R.id.buttonAgregarGrupo);

        buttonAgregarGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityFormularioGrupos = new Intent(v.getContext(), FormularioGrupo.class);
                //activityFormularioGrupos.putExtra("contactos", arrayContactos);
                startActivityForResult(activityFormularioGrupos,0);
            }
        });

        db = new OpenHelper(this);

        if(!db.obtenerGrupos().isEmpty()){
            arrayGrupos = db.obtenerGrupos();

            adapterGrupo = new ArrayAdapter<>(
                    this,
                    android.R.layout.simple_list_item_1,
                    db.obtenerGrupos()
            );

            listaGrupos.setAdapter(adapterGrupo);
        }else{
            Toast toast = Toast.makeText(getApplicationContext(), R.string.toastNoGrupos, Toast.LENGTH_LONG);
            toast.show();
        }

        listaGrupos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Grupo grupo = (Grupo)listaGrupos.getItemAtPosition(position);
                Intent activityListaAsistencia = new Intent(view.getContext(), ListaAsistencias.class);
                activityListaAsistencia.putExtra("nombreMateria", grupo.getClave_grupo());
                startActivityForResult(activityListaAsistencia,0);
                //finish();
            }
        });
    }
}
