package com.example.tiiko.a301403_proyecto.model;

public class Alumno {

    private int matricula;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;

    public Alumno(int matricula, String nombre, String apellido_paterno, String apellido_materno) {
        this.matricula = matricula;
        this.nombre = nombre;
        this.apellido_paterno = apellido_paterno;
        this.apellido_materno = apellido_materno;
    }

    public Alumno(int matricula) {
        this.matricula=matricula;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido_paterno() {
        return apellido_paterno;
    }

    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

    public String getApellido_materno() {
        return apellido_materno;
    }

    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }

    //Represetacion del objeto como cadena de texto
    public String toString() {
        String alumno = String.valueOf(matricula) + ": " + apellido_paterno + " " + apellido_materno + " " + nombre;
        return String.valueOf(matricula);
    }
}
