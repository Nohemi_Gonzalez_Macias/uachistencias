package com.example.tiiko.a301403_proyecto.model;

public class Asistencia {

    private String fecha_asistencia;
    private String tipo_asistencia;
    private int matricula;
    private String clave_grupo;

    public Asistencia(String fecha_asistencia, String tipo_asistencia, int matricula, String clave_grupo) {
        this.fecha_asistencia = fecha_asistencia;
        this.tipo_asistencia = tipo_asistencia;
        this.matricula = matricula;
        this.clave_grupo = clave_grupo;
    }

    public String getFecha_asistencia() {
        return fecha_asistencia;
    }

    public void setFecha_asistencia(String fecha_asistencia) {
        this.fecha_asistencia = fecha_asistencia;
    }

    public String getTipo_asistencia() {
        return tipo_asistencia;
    }

    public void setTipo_asistencia(String tipo_asistencia) {
        this.tipo_asistencia = tipo_asistencia;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getClave_grupo() {
        return clave_grupo;
    }

    public void setClave_grupo(String clave_grupo) {
        this.clave_grupo = clave_grupo;
    }

    //Represetacion del objeto como cadena de texto
    public String toString() {
        String object = "Fecha: " + fecha_asistencia + " Matricula: " + matricula;
        return object;
    }
}
