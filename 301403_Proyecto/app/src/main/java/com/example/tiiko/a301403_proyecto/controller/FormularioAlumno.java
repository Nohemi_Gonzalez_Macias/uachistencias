package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.Alumno;
import com.example.tiiko.a301403_proyecto.util.DeleteItemDialog;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class FormularioAlumno extends AppCompatActivity {

    private EditText editMatricula;
    private EditText editNombreAlumno;
    private EditText editApellidoPaterno;
    private EditText editApellidoMaterno;
    private EditText editClaveGrupo;
    private Button buttonAgregarALumno;
    private Button buttonVerAlumnos;
    String nombreMateria;

    //Lista de grupos y grupo actual
    private ArrayList<Alumno> listaAlumnos;
    private Alumno alunno;
    private ArrayAdapter spinnerAdapter;

    //Controlador de bases de datos
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_alumno);

        editMatricula=(EditText)findViewById(R.id.editMatricula);
        editNombreAlumno=(EditText)findViewById(R.id.editNombreAlumno);
        editApellidoPaterno=(EditText)findViewById(R.id.editApellidoPaterno);
        editApellidoMaterno=(EditText)findViewById(R.id.editApellidoMaterno);
        editClaveGrupo=(EditText)findViewById(R.id.editClaveGrupo);
        buttonAgregarALumno=(Button)findViewById(R.id.buttonAgregarAlumno);
        buttonVerAlumnos=(Button)findViewById(R.id.buttonVerAlumnos);

        Bundle bundle = getIntent().getExtras();
       nombreMateria = bundle.getString("nombreMateria");

        editClaveGrupo.setText(nombreMateria);
        editClaveGrupo.setEnabled(false);

        //Iniciamos el controlador de la base de datos
        db=new OpenHelper(this);

        // Agregar listeners
        buttonAgregarALumno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.insertarAlumno(
                        Integer.parseInt(editMatricula.getText().toString()),
                        editNombreAlumno.getText().toString(),
                        editApellidoPaterno.getText().toString(),
                        editApellidoMaterno.getText().toString()
                );

                db.insertarAlumnoGrupo(
                        Integer.parseInt(editMatricula.getText().toString()),
                        editClaveGrupo.getText().toString()
                );

                editMatricula.setText("");
                editNombreAlumno.setText("");
                editApellidoPaterno.setText("");
                editApellidoMaterno.setText("");
            }
        });

        buttonVerAlumnos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityListaALumnos = new Intent(v.getContext(), ListaAlumnos.class);
                activityListaALumnos.putExtra("nombreMateria", nombreMateria);
                startActivityForResult(activityListaALumnos,0);
            }
        });
    }
}
