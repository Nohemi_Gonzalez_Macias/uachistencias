package com.example.tiiko.a301403_proyecto.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.tiiko.a301403_proyecto.model.AlumnoGrupo;
import com.example.tiiko.a301403_proyecto.model.Asistencia;
import com.example.tiiko.a301403_proyecto.model.Grupo;

import java.util.ArrayList;

public class OpenHelper extends SQLiteOpenHelper {
    private static final String GRUPOS_TABLE_CREATE =
            "CREATE TABLE grupos(clave_grupo TEXT PRIMARY KEY, " +
                    "nombre_materia TEXT, " +
                    "porcentaje_faltas INT, " +
                    "horas_totales INT)";
    private static final String ALUMNOS_TABLE_CREATE =
            "CREATE TABLE alumnos(matricula INT PRIMARY KEY, " +
                    "nombre_alumno TEXT, " +
                    "apellido_paterno TEXT, " +
                    "apellido_materno TEXT)";
    private static final String ALUMNOS_GRUPOS_TABLE_CREATE =
            "CREATE TABLE alumnos_grupos(matricula INT, " +
                    "clave_grupo TEXT, " +
                    "FOREIGN KEY(matricula) REFERENCES alumnos(matricula)," +
                    "FOREIGN KEY(clave_grupo) REFERENCES grupos(clave_grupo)," +
                    "PRIMARY KEY(matricula, clave_grupo))";
    private static final String ASISTENCIAS_TABLE_CREATE =
            "CREATE TABLE asistencias(fecha_asistencia TEXT, " +
                    "tipo_asistencia TEXT, " +
                    "matricula INT, " +
                    "clave_grupo TEXT, " +
                    "FOREIGN KEY(matricula) REFERENCES alumnos(matricula)," +
                    "FOREIGN KEY(clave_grupo) REFERENCES grupos(clave_grupo)," +
                    "PRIMARY KEY(fecha_asistencia, matricula, clave_grupo))";
    private static final String DB_NAME =
            "uachistencias.sqlite";
    private static final int DB_VERSION = 1;
    private SQLiteDatabase db;

    public OpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = this.getWritableDatabase();
        db = this.getReadableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(GRUPOS_TABLE_CREATE);
        db.execSQL(ALUMNOS_TABLE_CREATE);
        db.execSQL(ALUMNOS_GRUPOS_TABLE_CREATE);
        db.execSQL(ASISTENCIAS_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    //Insertar un nuevo Grupo
    public String insertarGrupo(String clave_grupo, String nombre_materia, int porcentaje_faltas, int horas_totales) {
        try {
            ContentValues cv = new ContentValues();
            cv.put("clave_grupo", clave_grupo);
            cv.put("nombre_materia", nombre_materia);
            cv.put("porcentaje_faltas", porcentaje_faltas);
            cv.put("horas_totales", horas_totales);
            db.insert("grupos", null, cv);
        }catch(Exception e){
            return "Ya existe un grupo registrado con esa clave";
        }
        return null;
    }

    //Insertar un nuevo Alumno
    public void insertarAlumno(int matricula, String nombre_materia, String porcentaje_faltas, String horas_totales) {
        ContentValues cv = new ContentValues();
        cv.put("matricula", matricula);
        cv.put("nombre_alumno", nombre_materia);
        cv.put("apellido_paterno", porcentaje_faltas);
        cv.put("apellido_materno", horas_totales);
        db.insert("alumnos", null, cv);
    }

    //Insertar un nuevo Alumno Grupo
    public void insertarAlumnoGrupo(int matricula, String clave_grupo) {
        ContentValues cv = new ContentValues();
        cv.put("matricula", matricula);
        cv.put("clave_grupo", clave_grupo);
        db.insert("alumnos_grupos", null, cv);
    }

    //Insertar una nueva Asistencia
    public void insertarAsistencia(String fecha_asistencia, String tipo_asistencia, int matricula, String clave_grupo) {
        ContentValues cv = new ContentValues();
        //cv.put("clave_asistencia", null);
        cv.put("fecha_asistencia", fecha_asistencia);
        cv.put("tipo_asistencia", tipo_asistencia);
        cv.put("matricula", matricula);
        cv.put("clave_grupo", clave_grupo);
        db.insert("asistencias", null, cv);
    }

    //Obtener la lista de Grupos en la base de datos
    public ArrayList<Grupo> obtenerGrupos() {
        //Creamos el cursor
        ArrayList<Grupo> listaGrupos = new ArrayList<Grupo>();
        Cursor c = getReadableDatabase().rawQuery("select clave_grupo, nombre_materia, porcentaje_faltas, horas_totales from grupos", null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Grupo
                String clave_grupo = c.getString(c.getColumnIndex("clave_grupo"));
                String nombre_materia = c.getString(c.getColumnIndex("nombre_materia"));
                int porcentaje_faltas = c.getInt(c.getColumnIndex("porcentaje_faltas"));
                int horas_totales = c.getInt(c.getColumnIndex("horas_totales"));
                Grupo grupo = new Grupo(clave_grupo, nombre_materia, porcentaje_faltas, horas_totales);
                //Añadimos el Grupo a la lista
                listaGrupos.add(grupo);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listaGrupos;
    }

    //Obtener la lista de Grupos en la base de datos
    public ArrayList<Asistencia> obtenerAsistencia(String clave_grupo) {
        //Creamos el cursor
        ArrayList<Asistencia> listaAsistencia = new ArrayList<Asistencia>();
        Cursor c = getReadableDatabase().rawQuery("select fecha_asistencia, tipo_asistencia, matricula, clave_grupo from asistencias where clave_grupo like ?", new String[] {clave_grupo});
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Grupo
                String fecha_asistencia = c.getString(c.getColumnIndex("fecha_asistencia"));
                String tipo_asistencia = c.getString(c.getColumnIndex("tipo_asistencia"));
                int matricula = c.getInt(c.getColumnIndex("matricula"));
                String clave_grupo_columna = c.getString(c.getColumnIndex("clave_grupo"));
                Asistencia asistencia = new Asistencia(fecha_asistencia, tipo_asistencia, matricula, clave_grupo);
                //Añadimos el Grupo a la lista
                listaAsistencia.add(asistencia);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listaAsistencia;
    }

    public String obtenerClaveGrupo(String clave_grupo) {
        String claveGrupo;
        Cursor c = getReadableDatabase().rawQuery("select clave_grupo from grupos where clave_grupo like ?", new String[] {clave_grupo});
        c.moveToFirst();
        claveGrupo = c.getString(c.getColumnIndex("clave_grupo"));
        c.close();
        return claveGrupo;
    }

    public int obtenerFaltasPorAlumno() {
        int total_faltas = 0;
        Cursor c = getReadableDatabase().rawQuery("select count(fecha_asistencia) from asistencias where matricula = 1 and clave_grupo like '1' and tipo_asistencia like 'Falta';", null);
            c.moveToFirst();
            total_faltas = c.getInt(c.getColumnIndex("count(fecha_asistencia)"));
        c.close();
        return total_faltas;
    }

    public ArrayList<AlumnoGrupo> obtenerMatriculas(){
        //Creamos el cursor
        ArrayList<AlumnoGrupo> listaAlumnosGrupos = new ArrayList<AlumnoGrupo>();
        Cursor c = getReadableDatabase().rawQuery("select matricula, clave_grupo from alumnos_grupos", null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Grupo
                int matricula = c.getInt(c.getColumnIndex("matricula"));
                String clave_grupo = c.getString(c.getColumnIndex("clave_grupo"));
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(matricula, clave_grupo);
                //Añadimos el Grupo a la lista
                listaAlumnosGrupos.add(alumnoGrupo);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listaAlumnosGrupos;
    }

    public int obtenerFaltasPermitidasPorMateria() {
        int total_faltas = 0;
        Cursor c = getReadableDatabase().rawQuery("select count(fecha_asistencia) from asistencias where matricula = 1 and clave_grupo like '1' and tipo_asistencia like 'Falta';", null);
        c.moveToFirst();
        total_faltas = c.getInt(c.getColumnIndex("count(fecha_asistencia)"));
        c.close();
        return total_faltas;
    }

    //Obtener la lista de claves de grupos en la base de datos
    public ArrayList<AlumnoGrupo> obtenerClavesGrupos() {
        //Creamos el cursor
        ArrayList<AlumnoGrupo> listaAlumnosGrupos = new ArrayList<AlumnoGrupo>();
        Cursor c = getReadableDatabase().rawQuery("select clave_grupo from alumnos_grupos", null);
        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Grupo
                String clave_grupo = c.getString(c.getColumnIndex("clave_grupo"));
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(clave_grupo);
                //Añadimos el Grupo a la lista
                listaAlumnosGrupos.add(alumnoGrupo);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();
        return listaAlumnosGrupos;
    }

    //Obtener la lista de Alumnos en la base de datos
    public ArrayList<AlumnoGrupo> obtenerAlumnos(String clave_grupo) {
        //Creamos el cursor
        ArrayList<AlumnoGrupo> listaAlumnoGrupo = new ArrayList<AlumnoGrupo>();

        Cursor c = getReadableDatabase().rawQuery("select matricula, clave_grupo from alumnos_grupos where clave_grupo like ?", new String[]{clave_grupo});

        if (c != null && c.getCount() > 0) {
            c.moveToFirst();
            do {
                //Asignamos el valor en nuestras variables para crear un nuevo objeto Alumno
                int matricula = c.getInt(c.getColumnIndex("matricula"));
                String clave_grupo_columna = c.getString(c.getColumnIndex("clave_grupo"));
                AlumnoGrupo alumnoGrupo = new AlumnoGrupo(matricula, clave_grupo);
                //Añadimos el Alumno a la lista
                listaAlumnoGrupo.add(alumnoGrupo);
            } while (c.moveToNext());
        }

        //Cerramos el cursor
        c.close();

        return listaAlumnoGrupo;

    }

    //Borrar un Alumno a partir de su id
    public void borrar(String id) {
        String[] args = new String[]{id};
        db.delete("alumnos", "matricula=?", args);
    }

    //Borrar un Grupo a partir de su id
    public void borrarGrupo(String id) {
        String[] args = new String[]{id};
        db.delete("grupos", "clave_grupo=?", args);
    }
}