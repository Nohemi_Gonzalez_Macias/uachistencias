package com.example.tiiko.a301403_proyecto.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tiiko.a301403_proyecto.R;
import com.example.tiiko.a301403_proyecto.model.Grupo;
import com.example.tiiko.a301403_proyecto.util.DeleteItemDialog;
import com.example.tiiko.a301403_proyecto.util.OpenHelper;

import java.util.ArrayList;

public class FormularioGrupo extends AppCompatActivity {

    private EditText editGrupo;
    private EditText editMateria;
    private EditText editHorasTotales;
    private EditText editPorcentajeFaltas;
    private Button butonAgregarGrupo;
    private Button buttonVerGrupos;

    //Lista de grupos y grupo actual
    private ArrayList<Grupo> listaGrupos;
    private Grupo grupo;

    //Controlador de bases de datos
    private OpenHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_grupo);
        iniciar();
    }

    private void iniciar(){

        //Inicializamos los elementos de la interfaz
        editGrupo=(EditText)findViewById(R.id.editClaveGrupo);
        editMateria=(EditText)findViewById(R.id.editClaveMateria);
        editHorasTotales=(EditText)findViewById(R.id.editHorasTotales);
        editPorcentajeFaltas=(EditText)findViewById(R.id.editPorcentajeFaltas);
        butonAgregarGrupo=(Button)findViewById(R.id.buttonAgregarClaveGrupo);
        buttonVerGrupos=(Button)findViewById(R.id.buttonVerGrupos);

        //Iniciamos el controlador de la base de datos
        db=new OpenHelper(this);

        // Agregar listeners
        butonAgregarGrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if((db.obtenerClaveGrupo(editGrupo.getText().toString())).contains("Ya existe un grupo")){
                    Toast toast = Toast.makeText(getApplicationContext(), "Ya existe un grupo registrado con esa clave", Toast.LENGTH_LONG);
                    toast.show();
                } else {
                    db.insertarGrupo(
                            editGrupo.getText().toString(),
                            editMateria.getText().toString(),
                            Integer.parseInt(editHorasTotales.getText().toString()),
                            Integer.parseInt(editPorcentajeFaltas.getText().toString()));
                    editGrupo.setText("");
                    editMateria.setText("");
                    editHorasTotales.setText("");
                    editPorcentajeFaltas.setText("");
                }
            }
        });

        buttonVerGrupos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abrirFormularioGrupo(v);
            }
        });


    }

    private void abrirFormularioGrupo(View v){
        Intent grupoActivity = new Intent(v.getContext(), Grupos.class);
        startActivityForResult(grupoActivity,0);
    }
}
